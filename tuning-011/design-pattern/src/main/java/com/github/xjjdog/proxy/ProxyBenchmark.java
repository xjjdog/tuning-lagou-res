package com.github.xjjdog.proxy;


import com.github.xjjdog.proxy.cglibproxy.CglibFactory;
import com.github.xjjdog.proxy.jdkproxy.JdkFactory;
import com.github.xjjdog.proxy.jdkproxy.Target;
import com.github.xjjdog.proxy.jdkproxy.TargetImpl;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@Fork(2)
@State(Scope.Benchmark)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.Throughput)
public class ProxyBenchmark {

    Target jdkTarget = JdkFactory.newInstance(new TargetImpl());
    Target cglibTarget = CglibFactory.newInstance();

    @Benchmark
    public void jdk(Blackhole bh) {
        bh.consume(jdkTarget.targetMetod(4));
    }

    @Benchmark
    public void cglib(Blackhole bh) {
        bh.consume(cglibTarget.targetMetod(4));
    }

    public static void main(String[] args) throws Exception {
        Options opts = new OptionsBuilder()
                .include(ProxyBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .build();

        new Runner(opts).run();
    }
}
