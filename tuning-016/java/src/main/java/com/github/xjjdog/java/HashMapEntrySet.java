package com.github.xjjdog.java;

import java.util.Map;
import java.util.Set;

public class HashMapEntrySet {

    public void test(Map<String, String> map) {
        Set<Map.Entry<String, String>> set = map.entrySet();
        for (Map.Entry<String, String> entry : set) {
            entry.getKey();
            entry.getValue();
        }

        Set<String> set2 = map.keySet();
        for (String key : set2) {
            map.get(key);
        }
    }
}
