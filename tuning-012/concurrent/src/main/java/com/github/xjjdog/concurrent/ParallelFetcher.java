package com.github.xjjdog.concurrent;

import java.util.Map;
import java.util.concurrent.*;

public class ParallelFetcher {
    final long timeout;
    final CountDownLatch latch;
    final static ThreadPoolExecutor executor = new ThreadPoolExecutor(100, 200, 1,
            TimeUnit.HOURS, new ArrayBlockingQueue<>(100));

    public ParallelFetcher(int jobSize, long timeoutMill) {
        latch = new CountDownLatch(jobSize);
        timeout = timeoutMill;
    }

    public void submitJob(Runnable runnable) {
        executor.execute(() -> {
            runnable.run();
            latch.countDown();
        });
    }

    public void await() {
        try {
            this.latch.await(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            throw new IllegalStateException();
        }
    }

    public void dispose() {
        executor.shutdown();
    }

    public static void main(String[] args) {
        final String userid = "123";
        final SlowInterfaceMock mock = new SlowInterfaceMock();
        ParallelFetcher fetcher = new ParallelFetcher(20, 50);
        final Map<String, String> result = new ConcurrentHashMap<>();
//        final Map<String, String> result = new HashMap<>();

        fetcher.submitJob(() -> result.put("method0", mock.method0(userid)));
        fetcher.submitJob(() -> result.put("method1", mock.method1(userid)));
        fetcher.submitJob(() -> result.put("method2", mock.method2(userid)));
        fetcher.submitJob(() -> result.put("method3", mock.method3(userid)));
        fetcher.submitJob(() -> result.put("method4", mock.method4(userid)));
        fetcher.submitJob(() -> result.put("method5", mock.method5(userid)));
        fetcher.submitJob(() -> result.put("method6", mock.method6(userid)));
        fetcher.submitJob(() -> result.put("method7", mock.method7(userid)));
        fetcher.submitJob(() -> result.put("method8", mock.method8(userid)));
        fetcher.submitJob(() -> result.put("method9", mock.method9(userid)));
        fetcher.submitJob(() -> result.put("method10", mock.method10(userid)));
        fetcher.submitJob(() -> result.put("method11", mock.method11(userid)));
        fetcher.submitJob(() -> result.put("method12", mock.method12(userid)));
        fetcher.submitJob(() -> result.put("method13", mock.method13(userid)));
        fetcher.submitJob(() -> result.put("method14", mock.method14(userid)));
        fetcher.submitJob(() -> result.put("method15", mock.method15(userid)));
        fetcher.submitJob(() -> result.put("method16", mock.method16(userid)));
        fetcher.submitJob(() -> result.put("method17", mock.method17(userid)));
        fetcher.submitJob(() -> result.put("method18", mock.method18(userid)));
        fetcher.submitJob(() -> result.put("method19", mock.method19(userid)));

        fetcher.await();

        System.out.println(fetcher.latch);
        System.out.println(result.size());
        System.out.println(result);

        fetcher.dispose();
    }

}
