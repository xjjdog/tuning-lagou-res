public class Demo {
	static final class TestThread extends Thread {
		boolean stop = false;

		public boolean isStop() {
			return stop;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(100);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			stop = true;

			System.out.println("END");
		}
	}

	public static void main(String[] args) {
		int i = 0;
		TestThread test = new TestThread();
		test.start();
		while(!test.isStop()){
			System.out.println("--");
			i++;
		}
	}
}
//java -Djava.compiler=NONE  Demo
