package com.github.xjjdog.cache;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleRedisTest {

    @Autowired
    StringRedisTemplate redisTemplate;

    @Test
    public void simpleTest() {
        redisTemplate.opsForValue().set("test", "a");
        String value = String.valueOf(redisTemplate.opsForValue().get("test"));
        Assert.assertEquals(value, "a");
    }

}
