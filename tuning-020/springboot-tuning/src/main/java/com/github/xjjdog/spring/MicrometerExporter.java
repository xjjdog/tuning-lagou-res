package com.github.xjjdog.spring;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MicrometerExporter {
    @Autowired
    MeterRegistry registry;

    @GetMapping("/test")
    @ResponseBody
    public String test() {
        registry.counter("test",
                "from", "127.0.0.1",
                "method", "test"
        ).increment();

        return "ok";
    }
}
