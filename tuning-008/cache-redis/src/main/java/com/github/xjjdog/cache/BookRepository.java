package com.github.xjjdog.cache;

public interface BookRepository {

    Book getByIsbn(String isbn);

}