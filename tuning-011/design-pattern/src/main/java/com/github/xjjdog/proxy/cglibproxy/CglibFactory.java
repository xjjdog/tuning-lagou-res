package com.github.xjjdog.proxy.cglibproxy;

import com.github.xjjdog.proxy.jdkproxy.Target;
import com.github.xjjdog.proxy.jdkproxy.TargetImpl;
import org.springframework.cglib.proxy.Enhancer;

public class CglibFactory {

    public static Target newInstance() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(TargetImpl.class);
        enhancer.setCallback(new CglibInterceptor());
        return (Target) enhancer.create();
    }

    public static void main(String[] args) {
        Target target = newInstance();
        System.out.println(target.targetMetod(4));
    }
}
