package com.github.xjjdog.java.dsl;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Fork(2)
@State(Scope.Benchmark)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.Throughput)
public class RegexVsRagelBenchmark {

    ParserSQL regexModeParserSQL = new RegexModeParserSQL();
    ParserSQL ragelModeParserSQL = new RagelModeParserSQL();

    private static final String todo = "select * from USERS\n" +
            "where id>:smallId\n" +
            "##{\n" +
            " and FIRST_NAME like concat('%',:firstName,'%') }";

    @Benchmark
    @Threads(20)
    public void regex(Blackhole bh) {
        Map<String, Object> param = new HashMap<>();
        param.put("smallId",6);
        String sql = regexModeParserSQL.getFinalSQL(todo, param);
        bh.consume(sql);
    }

    @Benchmark
    @Threads(20)
    public void ragel(Blackhole bh) {
        Map<String, Object> param = new HashMap<>();
        param.put("smallId",6);
        String sql = ragelModeParserSQL.getFinalSQL(todo, param);
        bh.consume(sql);
    }

    public static void main(String[] args) throws Exception {
        Options opts = new OptionsBuilder()
                .include(RegexVsRagelBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .build();

        new Runner(opts).run();
    }
}
