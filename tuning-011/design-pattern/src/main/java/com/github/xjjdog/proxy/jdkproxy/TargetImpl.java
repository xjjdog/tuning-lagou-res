package com.github.xjjdog.proxy.jdkproxy;

public class TargetImpl implements Target {
    @Override
    public int targetMetod(int i) {
        return i * i;
    }
}
