package com.github.xjjdog.spring;

import org.springframework.stereotype.Component;

@Component
public class ABean {
    public void method() {
        System.out.println("*******************");
    }
}
