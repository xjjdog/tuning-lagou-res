package com.github.xjjdog.optimisticlock;

import com.github.xjjdog.spring.App;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class RedisLockV2Test {


    DefaultRedisScript script;

    @Before
    public void init() {
        script = new DefaultRedisScript();
        script.setScriptSource(new ResourceScriptSource(
                new ClassPathResource("unlock.lua")
        ));
        script.setResultType(Integer.class);
    }

    public String lock(String key, int timeOutSecond) {
        for (; ; ) {
            String stamp = String.valueOf(System.nanoTime());
            boolean exist = redisTemplate.opsForValue().setIfAbsent(key, stamp, timeOutSecond, TimeUnit.SECONDS);
            if (exist) {
                return stamp;
            }
        }
    }

    public void unlock(String key, String stamp) {
        redisTemplate.execute(script, Arrays.asList(key), stamp);
    }

    void realJob() {
        String resourceKey = "goodgirl";
        String stamp = lock(resourceKey, 10);
        try {
            //真正的业务
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new IllegalStateException();
            }
        } finally {
            unlock(resourceKey, stamp);
        }
    }


    @Autowired
    StringRedisTemplate redisTemplate;

    @Test
    public void simpleTest() {
        realJob();
    }

}
