package org.springframework.samples.petclinic.visit;

public class ThreadTest {
	public static void main(String[] args) {
		int i = 0;
		ThreadDemo t = new ThreadDemo();
		Thread tx = new Thread(t);
		tx.start();
		while (true){
			if(t.isFlag()){
				System.out.println("--");
				break;
			}
			i++;
		}
	}
}
class ThreadDemo implements Runnable{
	private boolean flag = false;

	@Override
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		flag = true;
		System.out.println("falg=" + isFlag());
	}

	public boolean isFlag(){
		return flag;
	}
}
