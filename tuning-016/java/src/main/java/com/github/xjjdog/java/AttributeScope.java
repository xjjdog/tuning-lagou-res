package com.github.xjjdog.java;

import org.springframework.util.StringUtils;

public class AttributeScope {
    public void test1(String str) {
        final int a = 100;
        if (!StringUtils.isEmpty(str)) {
            int b = a * a;
        }
    }

    public void test2(String str) {
        if (!StringUtils.isEmpty(str)) {
            final int a = 100;
            int b = a * a;
        }
    }
}
