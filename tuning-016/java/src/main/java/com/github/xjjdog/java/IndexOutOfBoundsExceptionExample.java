package com.github.xjjdog.java;

import java.util.List;

public class IndexOutOfBoundsExceptionExample {
    //BAD
    public String test1(List<String> list, int index) {
        try {
            return list.get(index);
        } catch (IndexOutOfBoundsException ex) {
            return null;
        }
    }

    //GOOD
    public String test2(List<String> list, int index) {
        if (index >= list.size() || index < 0) {
            return null;
        }
        return list.get(index);
    }

}
