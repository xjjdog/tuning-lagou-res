package com.github.xjjdog.java.dsl;

import java.util.Map;

interface ParserSQL {
    String ENDOFSQL = "\n";

    default String getFinalSQL(String sql, Map<String, Object> params) {

        //SQL处理和判空处理
        sql = sql.trim();
        sql = sql.replaceAll("\\s+", " ");
        sql = sql + ENDOFSQL;

        Map<String, String> transformMap = transform(sql, params);

        for (Map.Entry<String, String> e : transformMap.entrySet()) {
            sql = sql.replace(e.getKey(), e.getValue());
        }
        return sql;
    }


    Map<String, String> transform(String sql, Map<String, Object> params);

}
