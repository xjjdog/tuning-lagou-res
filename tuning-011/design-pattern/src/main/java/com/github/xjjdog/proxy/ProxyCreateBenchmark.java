package com.github.xjjdog.proxy;


import com.github.xjjdog.proxy.cglibproxy.CglibFactory;
import com.github.xjjdog.proxy.jdkproxy.JdkFactory;
import com.github.xjjdog.proxy.jdkproxy.TargetImpl;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@Fork(2)
@State(Scope.Benchmark)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.Throughput)
public class ProxyCreateBenchmark {


    @Benchmark
    public void jdk(Blackhole bh) {
        bh.consume(JdkFactory.newInstance(new TargetImpl()));
    }

    @Benchmark
    public void cglib(Blackhole bh) {
        bh.consume(CglibFactory.newInstance());
    }

    public static void main(String[] args) throws Exception {
        Options opts = new OptionsBuilder()
                .include(ProxyCreateBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .build();

        new Runner(opts).run();
    }
}
