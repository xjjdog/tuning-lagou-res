package com.github.xjjdog.tuning;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

@BenchmarkMode(Mode.SingleShotTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 0, time = 1, timeUnit = TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(1)
@Threads(6)
public class SingleShotTimeTest {


    int a;

    @Benchmark
    @Group("a")
    public void test() {
        try {
            a++;
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @TearDown
    public void aTest(){
        System.out.println("============="+a);
    }

    public static void main(String[] args) throws Exception {
        Options opts = new OptionsBuilder()
                .include(SingleShotTimeTest.class.getSimpleName())
                .resultFormat(ResultFormatType.JSON)
                .build();

        new Runner(opts).run();
    }
}
