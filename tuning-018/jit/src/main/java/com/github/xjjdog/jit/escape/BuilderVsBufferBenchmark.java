package com.github.xjjdog.jit.escape;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@Fork(value = 2, jvmArgs = "-XX:+EliminateLocks")
@State(Scope.Benchmark)
@Warmup(iterations = 5, time = 1)
@Measurement(iterations = 5, time = 1)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@BenchmarkMode(Mode.Throughput)

public class BuilderVsBufferBenchmark {

    @Benchmark
    @Threads(20)
    public void builder(Blackhole bh) {
        StringBuilder sb = new StringBuilder();
        sb.append("1");
        sb.append("2");
        sb.append(3);
        sb.append(4);
        bh.consume(sb.toString());
    }

    @Benchmark
    @Threads(20)
    public void buffer(Blackhole bh) {
        StringBuffer sb = new StringBuffer();
        sb.append("1");
        sb.append("2");
        sb.append(3);
        sb.append(4);
        bh.consume(sb.toString());
    }


    public static void main(String[] args) throws Exception {
        Options opts = new OptionsBuilder()
                .include(BuilderVsBufferBenchmark.class.getSimpleName())
                .resultFormat(ResultFormatType.CSV)
                .build();

        new Runner(opts).run();
    }
}
