package com.github.xjjdog.optimisticlock;

import com.github.xjjdog.spring.App;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class RedisLockV1Test {

    public void lock(String key, int timeOutSecond) {
        for (; ; ) {
            boolean exist = redisTemplate.opsForValue().setIfAbsent(key, "", timeOutSecond, TimeUnit.SECONDS);
            if (exist) {
                break;
            }
        }
    }

    public void unlock(String key) {
        redisTemplate.delete(key);
    }

    void realJob() {
        String resourceKey = "goodgirl";
        lock(resourceKey, 10);
        try {
            //真正的业务
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new IllegalStateException();
            }
        } finally {
            unlock(resourceKey);
        }
    }


    @Autowired
    StringRedisTemplate redisTemplate;

    @Test
    public void simpleTest() {
        realJob();
    }

}
