package com.github.xjjdog.proxy.jdkproxy;

import java.lang.reflect.Proxy;

public class JdkFactory {
    public static Target newInstance(Target target) {
        Object object = Proxy.newProxyInstance(JdkInvocationHandler.class.getClassLoader(),
                new Class<?>[]{Target.class},
                new JdkInvocationHandler(target));
        return Target.class.cast(object);
    }

    public static void main(String[] args) {
        Target t = new TargetImpl();
        Target target = newInstance(t);
        System.out.println(target.targetMetod(4));
    }
}
