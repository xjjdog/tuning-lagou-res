package com.github.xjjdog.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
    @Autowired
    AsyncJob asyncJob;

    @GetMapping("/test")
    @ResponseBody
    public String testJob() throws Exception{
        System.out.println(asyncJob.testJob());
        System.out.println(asyncJob.testJob2().get());
        return "ok";
    }
}
