package com.github.xjjdog.java.dsl;

class Pair {
    String namedParams;
    String quote;

    public String getNamedParams() {
        return namedParams;
    }

    public void setNamedParams(String namedParams) {
        this.namedParams = namedParams;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}
