package com.github.xjjdog.optimisticlock;

import com.github.xjjdog.spring.App;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class RedisLockV3Test {


    RedissonClient redisson;

    @Before
    public void init() {
        Config cfg = new Config();
        cfg.useMasterSlaveServers().setMasterAddress("redis://localhost:6379");
        redisson = Redisson.create(cfg);
    }

    @Test
    public void realJob() {
        String resourceKey = "goodgirl";
        RLock lock = redisson.getLock(resourceKey);
        try {
            lock.lock(5, TimeUnit.SECONDS);
            //真正的业务
            Thread.sleep(100);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (lock.isLocked()) {
                lock.unlock();
            }

        }
    }

    @Autowired
    StringRedisTemplate redisTemplate;

    @Test
    public void simpleTest() {
        realJob();
    }

}
