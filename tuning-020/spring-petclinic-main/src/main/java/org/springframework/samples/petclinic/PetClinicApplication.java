/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic;

import org.apache.coyote.http11.Http11Nio2Protocol;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.cache.annotation.EnableCaching;

/**
 * PetClinic Spring Boot Application.
 *
 * @author Dave Syer
 */
@SpringBootApplication(proxyBeanMethods = false)
@EnableCaching
public class PetClinicApplication implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
	public static void main(String[] args) {
		SpringApplication.run(PetClinicApplication.class, args);
	}


	@Override
	public void customize(ConfigurableServletWebServerFactory factory) {
		TomcatServletWebServerFactory f = (TomcatServletWebServerFactory) factory;
//		f.setProtocol("org.apache.coyote.http11.Http11Nio2Protocol");
		f.setProtocol("org.apache.coyote.http11.Http11Nio2Protocol");
		f.addConnectorCustomizers(c -> {
			Http11Nio2Protocol protocol = (Http11Nio2Protocol) c.getProtocolHandler();
			protocol.setMaxConnections(200);
			protocol.setMaxThreads(200);
			protocol.setSessionTimeout(3000);
			protocol.setConnectionTimeout(3000);
		});
	}
}


